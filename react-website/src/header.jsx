import React, { Component } from 'react';
import './header.css';
import logo from './gallery/logo_dog.jpg'

class Header extends Component {
  render() {
    return (
        <header className="App-header">
          <img src={logo} className="App-logo" alt="doggo" />
          <nav className="nav-menu">
            <ul className="nav-list">
              <li className="inline-listItem"><a href="webpage.html">Home</a></li>
              <li className="inline-listItem"><a href="About.html">About Us</a></li>
              <li className="inline-listItem"><a href="#">Membership</a></li>
              <li className="inline-listItem"><a href="#">Contact</a></li>
            </ul>
          </nav>
          <form>
            <label className="search"><span className="glyphicon glyphicon-search"></span> Search:</label>
            <input type="text" name="Search" placeholder="Search Here"/>
            <input className="button" type="submit" value="Woof!"/>
            <input className="button login" type="submit" value="Sign In"/>
          </form>
        </header>
    );
  }
}

export default Header;
